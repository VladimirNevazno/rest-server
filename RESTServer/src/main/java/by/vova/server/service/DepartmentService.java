package by.vova.server.service;

import java.util.List;

import by.vova.server.domain.Department;
import by.vova.server.domain.Employee;

public interface DepartmentService {

	public Department addDepartment(Department department);

	public List<Department> listDepartment();
	
	public List<Employee> getEmployeesInDepartment(Integer id);
	
	public Department removeDepartment(Integer id);
	
	public Department removeDepartment(Department department);
	
	public Department getDepartment(Integer id);

}

package by.vova.server.service;
 
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import by.vova.server.dao.DepartmentDAO;
import by.vova.server.domain.Department;
import by.vova.server.domain.Employee;

 
@Service
public class DepartmentServiceImpl implements DepartmentService {
 
    private DepartmentDAO departmentDAO;
    
    @Autowired
	public DepartmentServiceImpl(DepartmentDAO departmentDAO) {
		this.departmentDAO = departmentDAO;
	}
	
/*-------------------------------------------------------------------------------------*/
    //1
	public Department addDepartment(Department department) {
		return departmentDAO.addDepartment(department);
	}

	//2
	public List<Department> listDepartment() {
		return departmentDAO.listDepartments();
	}
    
	//3
	public Department removeDepartment(Integer id) {
		return departmentDAO.removeDepartment(id);
	}
    
	//4
	public Department removeDepartment(Department department) {
		return departmentDAO.removeDepartment(department);
	}

	//5
	public Department getDepartment(Integer id) {
		return departmentDAO.getDepartment(id);
	}

	//6
	public List<Employee> getEmployeesInDepartment(Integer id) {
		return departmentDAO.getEmployeesInDepartment(id);
	}
	
    
}

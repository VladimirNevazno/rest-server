package by.vova.server.service;
 
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import by.vova.server.dao.EmployeeDAO;
import by.vova.server.domain.Employee;

 
@Service
public class EmployeeServiceImpl implements EmployeeService {
 
    private EmployeeDAO employeeDAO;

    @Autowired
    public EmployeeServiceImpl(EmployeeDAO employeeDAO) {
    	this.employeeDAO = employeeDAO;
    }
    
/*-------------------------------------------------------------------------------------*/    
    
    /*1*/
    public Employee addEmployee(Employee employee) {
        return employeeDAO.addEmployee(employee);
    }
 
    /*2*/
    public List<Employee> listEmployees() {
 
        return employeeDAO.listEmployees();
    }
 
    /*3*/
	public Employee getEmployee(Integer id) {
		return employeeDAO.getEmployee(id);
	}
    
    /*4*/
    public Employee removeEmployee(Integer id) {
        return employeeDAO.removeEmployee(id);
    }

	/*5*/
	public Employee removeEmployee(Employee employee) {
		return employeeDAO.removeEmployee(employee);
	}

    
}

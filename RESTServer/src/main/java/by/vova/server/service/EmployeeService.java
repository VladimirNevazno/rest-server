package by.vova.server.service;

import java.util.List;

import by.vova.server.domain.Employee;


public interface EmployeeService {

	public Employee addEmployee(Employee employee);

	public List<Employee> listEmployees();

	public Employee getEmployee(Integer id);
	
	public Employee removeEmployee(Integer id);
	
	public Employee removeEmployee(Employee employee);
}

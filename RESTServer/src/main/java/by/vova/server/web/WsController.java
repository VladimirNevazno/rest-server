package by.vova.server.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import by.vova.server.domain.Department;
import by.vova.server.domain.Employee;
import by.vova.server.service.DepartmentService;
import by.vova.server.service.EmployeeService;

/*веб-сервис*/
@Controller
@RequestMapping("/rs")
public class WsController {
	
	private DepartmentService departmentService;
	
	private EmployeeService employeeService;
	
	@Autowired
	public WsController(DepartmentService departmentService, EmployeeService employeeService) {
		this.departmentService = departmentService;
		this.employeeService = employeeService;
	}
	
/*----------------------------------------------------------------------------------------------------------------------------------*/	
	
	/*1*/
	/*отделы со средней зарплатой*/
	@RequestMapping(value = "/department", method = RequestMethod.GET, headers = {"Accept=text/xml, application/json"})
	public @ResponseBody List<Department> getDepartmentsWithAverageSalary() {
		return departmentService.listDepartment();
	}
	
	/*2*/
	/*выдать отдел*/
	@RequestMapping(value = "/department/{id}", method = RequestMethod.GET, headers = {"Accept=text/xml, application/json"})
	public @ResponseBody Department getDepartment(@PathVariable("id") Integer departmentId) {
		
		return departmentService.getDepartment(departmentId);
	}
	
	/*3*/
	/*сотрудники в отделе*/
	@RequestMapping(value="/department/employees/{id}", method=RequestMethod.GET, headers={"Accept=text/xml, application/json"})
	public @ResponseBody List<Employee> getEmpInDep(@PathVariable("id") Integer id) {
		return departmentService.getEmployeesInDepartment(id);
	}
	
	/*4*/
	/*создать новый отдел*/
	@RequestMapping(value="/department/add", method=RequestMethod.POST, headers={"Accept=text/xml, application/json"})
	public @ResponseBody Department addDepartment(@RequestBody Department department) {
		Department result = departmentService.addDepartment(department);
		return result;
	}
	
	/*5*/
	/*создать нового сотрудника*/
	@RequestMapping(value="/employee/add/{id}", method=RequestMethod.POST, headers={"Accept=text/xml, application/json"})
	public @ResponseBody Employee addEmployee(@RequestBody Employee employee, @PathVariable("id") Integer id) {
		employee.setDepartment(departmentService.getDepartment(id));
		Employee result = employeeService.addEmployee(employee);
		return result;
	}
	
	/*6*/
	/*получить сотрудника*/
	@RequestMapping(value="/employee/{id}", method=RequestMethod.GET, headers={"Accept=text/xml, application/json"})
	public @ResponseBody Employee getEmployee(@PathVariable("id") Integer id) {
		
		Employee result = employeeService.getEmployee(id);
		return result;
	}
	
	/*7*/
	/*удаление отдела*/
	@RequestMapping(value="/department/delete/{id}", method=RequestMethod.GET, headers={"Accept=text/xml, application/json"})
	public @ResponseBody Department deleteDepartment(@PathVariable("id") Integer id) {
		
		Department result = departmentService.removeDepartment(id);
		
		return result;
	}
	
	/*8*/
	/*удаление сотрудника*/
	@RequestMapping(value="/employee/delete/{id}", method=RequestMethod.GET, headers={"Accept=text/xml, application/json"})
	public @ResponseBody Employee deleteEmployee(@PathVariable("id") Integer id) {
		
		Employee result = employeeService.removeEmployee(id);
		
		return result;
	}
}

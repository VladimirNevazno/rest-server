package by.vova.server.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import by.vova.server.domain.Employee;


@Repository
public class EmployeeDAOImpl implements EmployeeDAO {
	
	private SessionFactory sessionFactory;

	
	@Autowired
	public EmployeeDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
/*------------------------------------------------------------------------------------------------*/
	
	private Session getSession() {
		return sessionFactory.getCurrentSession();
	}
	
	/*1*/
	@Transactional
	public Employee addEmployee(Employee employee) {
		
		getSession().saveOrUpdate(employee);
		
		return employee;
	}

	/*2*/
	@Transactional
	@SuppressWarnings("unchecked")
	public List<Employee> listEmployees() {
			return getSession().createQuery("from Employee").list();
	}
	
	/*3*/
	@Transactional
	public Employee getEmployee(Integer id) {
		return (Employee) getSession().get(Employee.class, id);
	}

	/*4*/
	@Transactional
	public Employee removeEmployee(Integer id) {
		
		Employee employee = (Employee) getSession().load(Employee.class, id);
		
		return removeEmployee(employee);
	}

	/*5*/
	@Transactional
	public Employee removeEmployee(Employee employee) {
		
		if (null != employee) {
			
			getSession().delete(employee);
			
		}
		return employee;
	}
	
	
}

package by.vova.server.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import by.vova.server.domain.Department;
import by.vova.server.domain.Employee;


@Repository
public class DepartmentDAOImpl implements DepartmentDAO {
	
	private static final Logger logger = LoggerFactory.getLogger(DepartmentDAOImpl.class);
	
	private SessionFactory sessionFactory;
	
	
	@Autowired
	public DepartmentDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

/*------------------------------------------------------------------------------------------------*/
	
	private Session getSession() {
		return sessionFactory.getCurrentSession();
	}
	
	/*1. */
	@Transactional
	public Department addDepartment(Department department) {
		
		getSession().saveOrUpdate(department);
		
		logger.info("сохранили или обновили отдел: "+department);
		return department;
	}
	
	/*2*/
	@Transactional
	@SuppressWarnings("unchecked")
	public List<Department> listDepartments() {
		
		logger.info("вернули список отделов с сортировкой по имени.");
		
		return getSession().createSQLQuery("SELECT ID, (select avg(salary) from employees e where e.department_ID = d.ID) as AVERAGE_SALARY, NAME FROM departments d;").addEntity(Department.class).list();
	}
	
	/*4*/
	@Transactional
	public Department getDepartment(Integer id) {
		logger.info("возвращаем отдел.");
		return (Department) getSession().get(Department.class, id);
	}
	
	/*5*/
	@Transactional
	public List<Employee> getEmployeesInDepartment(Integer id) {
		List<Employee> result = null;
		
		Department department = (Department) getSession().get(Department.class, id);
		if (department != null && department.getEmployees() != null && !department.getEmployees().isEmpty()) {
			
			result = department.getEmployees();
		}
		logger.info("вернули список сотрудников отдела.");
		return result;
	}
	
	/*6*/
	@Transactional
	public Department removeDepartment(Integer id) {
		Session session = getSession();
		Department department = (Department) session.load(Department.class, id);
		
		if (null != department) {
			session.delete(department);
			logger.info("удалили отдел.");
		}
		
		return department;
	}
	
	/*7*/
	@Transactional
	public Department removeDepartment(Department department) {
		Session session = getSession();
		if (null != department) {
			session.delete(department);
			logger.info("удалили отдел.");
		}
		
		return department;
	}
	
}

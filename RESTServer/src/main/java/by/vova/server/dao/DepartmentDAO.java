package by.vova.server.dao;

import java.util.List;

import by.vova.server.domain.Department;
import by.vova.server.domain.Employee;

public interface DepartmentDAO {
	
	public Department addDepartment(Department department);
	
	public List<Department> listDepartments();
	
	public Department getDepartment(Integer id);
	
	public List<Employee> getEmployeesInDepartment(Integer id);
	
	public Department removeDepartment(Integer id);
	
	public Department removeDepartment(Department department);
	
}
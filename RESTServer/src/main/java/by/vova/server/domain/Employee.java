package by.vova.server.domain;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Entity
@Table(name = "EMPLOYEES")
public class Employee {

	private static final Logger logger = LoggerFactory.getLogger(Employee.class);
	
	@Id
	@Column(name = "ID")
	@GeneratedValue
	private Integer id;

	@Column(name = "FIRSTNAME")
	private String firstname;

	@Column(name = "LASTNAME")
	private String lastname;
	
	@Column(name = "PATRONYMIC")
	private String patronymic;

	@Column(name = "SALARY")
	private Integer salary;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false, cascade = CascadeType.REFRESH)
	private Department department;
	/* (optional = false) Поле department(_id) не может быть null */
	/* cascade = CascadeType.REFRESH позволяет создавать сотрудника передавая ему отдел через конструктор.
	 * необходимо будет обновить связи с помощью session.refresh(employee)//1 или session.refresh(department)//2;
	 * для связывания объектов. Причём при перечитывании (refresh) объекта в первом (//1) случае используются касадные связи сотрудника, во втором - отдела.*/
	
	public Employee() {
	}

	/*конструктор специально для теста. id назначается Hibernate*/
	public Employee(Integer id, String firstname, String lastname, String patronymic,
			Integer salary, Department department) {
		this.id = id;
		this.firstname = firstname;
		this.lastname = lastname;
		this.patronymic = patronymic;
		this.salary = salary;
		this.department = department;
	}
	
	public Employee(String firstname, String lastname, String patronymic,
			Integer salary, Department department) {
		this.firstname = firstname;
		this.lastname = lastname;
		this.patronymic = patronymic;
		this.salary = salary;
		this.department = department;
	}
	
	// Getters and setters
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getPatronymic() {
		return patronymic;
	}

	public void setPatronymic(String patronymic) {
		this.patronymic = patronymic;
	}

	public Integer getSalary() {
		return salary;
	}

	public void setSalary(Integer salary) {
		this.salary = salary;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	@Override
	public String toString() {
		return "Employee [id=" + id + ", firstname=" + firstname + ", lastname=" + lastname + ", patronymic=" + patronymic
				+ ", salary=" + salary + /*", department=" + department + */"]";
	}

	@Override
	public boolean equals(Object obj) {
		
		if (this == obj) {logger.debug("this == obj"); return true;}
        if (obj == null) {logger.debug("obj == null"); return false;}
        if (this.getClass() != obj.getClass()) {
        	logger.debug("getClass() != obj.getClass()\nthis.getClass(): "+this.getClass()+"\n != \nobj.getClass(): "+obj.getClass());
        	return false;
        	}
        
        Employee other = (Employee) obj;
        if (!this.id.equals(other.getId())) {logger.debug("id = false"); return false;}
        if (!this.firstname.equals(other.getFirstname())) {logger.debug("firstname = false"); return false;}
        if (!this.lastname.equals(other.getLastname())) {logger.debug("lastname = false"); return false;}
        if (!this.patronymic.equals(other.getPatronymic())) {logger.debug("patronymic = false"); return false;}
        
        if (this.salary != null && other.getSalary() != null) {
        	logger.debug("this salary = "+this.salary+", expect salary = "+other.getSalary());
        	if (!this.salary.equals(other.getSalary())) {logger.debug("salary = false"); return false;}
        }
       
        return true;
	}
	
	
}

package by.vova.server.domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name = "DEPARTMENTS", uniqueConstraints = {@UniqueConstraint(columnNames = "name")})
public class Department {
	
	private static final Logger logger = LoggerFactory.getLogger(Department.class);
	
	@Id
	@Column(name = "ID")
	@GeneratedValue
	private Integer id;

	@Column(name = "NAME")
	private String name;
	
	@Column(name = "AVERAGE_SALARY")
	private Integer averageSalary;
	
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, orphanRemoval = true, cascade = CascadeType.ALL, mappedBy = "department")
	List<Employee> employees = null;
	/*CascadeType.ALL необходим для сохранения и обновления коллекции при сохранении или обновлении сущностей в List.*/
	/*cascade = {CascadeType.ALL, CascadeType.MERGE}*/
	/*orphanRemoval = true (“удаление объектов-сирот”)*/
	/*где mappedBy = «department» — это имя поля в классе Employee*/
	/*где FetchType.LAZY — говорит нам о том, что коллекцию мы будем загружать данными только по требованию.*/

	public Department() {
		
	}
	
	/*конструктор специально для теста. id при работе приложения назначается Hibernate*/
	public Department(Integer id, String name, Integer averageSalary,
			List<Employee> employees) {
		this.id = id;
		this.name = name;
		this.averageSalary = averageSalary;
		this.employees = employees;
	}

	public Department(String name, Integer averageSalary) {
		this.name = name;
		this.averageSalary = averageSalary;
	}
	
	public Department(String name) {
		this.name = name;
	}

/*---------------------------------------------------------------------*/	
	
	// Getters and setters
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAverageSalary() {
		return averageSalary;
	}

	public void setAverageSalary(Integer averageSalary) {
		this.averageSalary = averageSalary;
	}

	public List<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}

	@Override
	public String toString() {
		return "Department [id=" + id + ", name=" + name + ", averageSalary="+ averageSalary +"]";
	}
	
	@Override
	public boolean equals(Object obj) {
		
		if (this == obj) {logger.debug("this == obj"); return true;}
        if (obj == null) {logger.debug("obj == null"); return false;}
        if (this.getClass() != obj.getClass()) {
        	logger.debug("getClass() != obj.getClass()\nthis.getClass(): "+this.getClass()+"\n != \nobj.getClass(): "+obj.getClass());
        	return false;
        	}
        
        Department other = (Department) obj;
        if (!this.id.equals(other.getId())) {logger.debug("id = false"); return false;}
        if (!this.name.equals(other.getName())) {logger.debug("name = false"); return false;}
        if (this.averageSalary != null && other.getAverageSalary() != null) {
        	logger.debug("this as = "+this.averageSalary+"expect as = "+other.getAverageSalary());
        	if (!this.averageSalary.equals(other.getAverageSalary())) {logger.debug("as = false"); return false;}
        }
       
        return true;
	}
	
}

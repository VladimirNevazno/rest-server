package by.vova.server.web;

import static org.junit.Assert.*;

import org.junit.Test;
import org.springframework.ui.ModelMap;


public class IndexControllerTest {

	@Test
	public void testIndex() {
		IndexController controller = new IndexController();
		ModelMap model = new ModelMap();
		String viewName = controller.index(model);
		assertEquals("index", viewName);
	}

	@Test
	public void testInd() {
		IndexController controller = new IndexController();
		ModelMap model = new ModelMap();
		String viewName = controller.ind(model);
		assertEquals("redirect:/", viewName);
	}

}

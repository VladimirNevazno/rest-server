package by.vova.server.web;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.List;

import org.junit.Test;

import by.vova.server.domain.Department;
import by.vova.server.domain.Employee;
import by.vova.server.service.DepartmentService;
import by.vova.server.service.EmployeeService;


public class WsControllerTest {
	
	private List<Department> departments = null;
	private Department department = null;
	private List<Employee> employees = null;
	private Employee employee = null;
	
	private static Integer id = 1;
	
	private DepartmentService departmentService = mock(DepartmentService.class);
	private EmployeeService employeeService = mock(EmployeeService.class);
	private WsController WsController = new WsController(departmentService, employeeService);
	
/*-----------------------------------------------------------------------------------------------*/
	
	/*1*/
	@Test
	public void testGetDepartmentsWithAverageSalary() {
		
		when(departmentService.listDepartment()).thenReturn(departments);
		
		List<Department> result = WsController.getDepartmentsWithAverageSalary();
		
		assertSame(departments, result);
		verify(departmentService).listDepartment();
	}

	/*2*/
	@Test
	public void testGetDepartment() {
		when(departmentService.getDepartment(id)).thenReturn(department);
		
		Department result = WsController.getDepartment(id);
		
		assertSame(department, result);
		verify(departmentService).getDepartment(id);
	}
	
	/*3*/
	@Test
	public void testGetEmpInDep() {
		
		when(departmentService.getEmployeesInDepartment(id)).thenReturn(employees);
		
		List<Employee> result = WsController.getEmpInDep(id);
		
		assertSame(employees, result);
		verify(departmentService).getEmployeesInDepartment(id);
	}
	
	/*4*/
	@Test
	public void testAddDepartment() {
		when(departmentService.addDepartment(department)).thenReturn(department);
		
		Department result = WsController.addDepartment(department);
		
		assertSame(department, result);
		verify(departmentService).addDepartment(department);
	}
	
	/*5*/
	@Test
	public void testAddEmployee() {
		
		department = new Department();
		employee = new Employee();
		
		when(departmentService.getDepartment(id)).thenReturn(department);
		when(employeeService.addEmployee(employee)).thenReturn(employee);
		
		Employee result = WsController.addEmployee(employee, id);
		
		
		verify(departmentService).getDepartment(id);
		verify(employeeService).addEmployee(employee);
		assertSame(employee.getDepartment(), department);//проверяем добавился ли отдел к сотруднику
		assertSame(employee, result);
	}
	
	/*6*/
	@Test
	public void testGetEmployee() {
		when(employeeService.getEmployee(id)).thenReturn(employee);
		
		Employee result = WsController.getEmployee(id);
		
		assertSame(employee, result);
		verify(employeeService).getEmployee(id);
	}
	
	/*7*/
	@Test
	public void testDeleteDepartment() {
		
		department = new Department(333, "name", 444, employees);
		
		when(departmentService.removeDepartment(id)).thenReturn(department);
		
		Department result = WsController.deleteDepartment(id);
		
		assertEquals(department, result);
	}
	
	/*8*/
	@Test
	public void testDeleteEmployee() {
		
		Department department = new Department(333, "name", 444, employees);
		employee = new Employee(44, "firstname", "lastname", "patronymic", 777, department);
		
		when(employeeService.removeEmployee(id)).thenReturn(employee);
		
		Employee result = WsController.deleteEmployee(id);
		
		assertEquals(employee, result);
	}

}

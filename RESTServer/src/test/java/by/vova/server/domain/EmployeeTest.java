package by.vova.server.domain;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

public class EmployeeTest {

	private static Department department;
	private static Employee employee;
	private static Employee employee2;

	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
		department = new Department("Спортивный отдел");
		
		employee = new Employee(341, "Алексей", "Смирнов", "Викторович", 12345, department);
		employee2 = new Employee();
	}
	
/*---------------------------------------------------------------------*/
	//getters
	@Test
	public void testGetId() {
		assertTrue(employee.getId() == 341);
	}

	@Test
	public void testGetFirstname() {
		assertEquals(employee.getFirstname(), "Алексей");
	}

	@Test
	public void testGetLastname() {
		assertEquals(employee.getLastname(), "Смирнов");
	}

	@Test
	public void testGetPatronymic() {
		assertEquals(employee.getPatronymic(), "Викторович");
	}
	
	@Test
	public void testGetSalary() {
		assertTrue(employee.getSalary() == 12345);
	}

	@Test
	public void testGetDepartment() {
		assertEquals(employee.getDepartment(), department);
	}
	
	//setters
	/*в set-методах используются get-методы для получения значений*/
	@Test
	public void testSetId() {
		employee2.setId(555);
		assertTrue(employee2.getId() == 555);
	}

	@Test
	public void testSetFirstname() {
		employee2.setFirstname("Петька");
		assertEquals(employee2.getFirstname(), "Петька");
	}

	@Test
	public void testSetLastname() {
		employee2.setLastname("Иванов");
		assertEquals(employee2.getLastname(), "Иванов");
	}

	@Test
	public void testSetPatronymic() {
		employee2.setPatronymic("Владимирович");
		assertEquals(employee2.getPatronymic(), "Владимирович");
	}
	
	@Test
	public void testSetSalary() {
		employee2.setSalary(9873);
		assertTrue(employee2.getSalary() == 9873);
	}

	@Test
	public void testSetDepartment() {
		employee2.setDepartment(department);
		assertEquals(employee2.getDepartment(), department);
	}

}

package by.vova.server.domain;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

public class DepartmentTest {
	
	private static Department department;
	private static Department department2;
	private static List<Employee> employees;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
		Employee employee = new Employee(777, "Вася", "Иванов", "Петрович", 1000, department);
		Employee employee2 = new Employee(345, "Владимир", "Путин", "Владимирович", 500, department);
		Employee employee3 = new Employee(9145, "Саша", "Пушкин", "Сергеевич", 600, department);
		
		employees = new ArrayList<Employee>();
		employees.add(employee);
		employees.add(employee2);
		employees.add(employee3);
		
		department = new Department(444, "Особый отдел", 700, employees);
		department2 = new Department();
	}

/*---------------------------------------------------------------------*/	
	//getters
	
	@Test
	public void testGetId() {
		assertTrue(department.getId() == 444);
	}

	@Test
	public void testGetName() {
		assertEquals(department.getName(), "Особый отдел");
	}

	@Test
	public void testGetAverageSalary() {
		assertTrue(department.getAverageSalary() == 700);
	}
	
	@Test
	public void testGetEmployees() {
		assertEquals(department.getEmployees(), employees);
		assertTrue(department.getEmployees() == employees);
	}
	
	//setters
	/*в set-методах используются get-методы для получения значений*/
	@Test
	public void testSetId() {
		department2.setId(555);
		assertTrue(department2.getId() == 555);
	}
	
	@Test
	public void testSetName() {
		department2.setName("Оранжевых слив");
		assertEquals(department2.getName(), "Оранжевых слив");
	}
	
	@Test
	public void testSetAverageSalary() {
		department2.setAverageSalary(543);
		assertTrue(department2.getAverageSalary() == 543);
		assertTrue(department2.getAverageSalary().equals(543));
	}

	@Test
	public void testSetEmployees() {
		department2.setEmployees(employees);
		assertEquals(department2.getEmployees(), employees);
	}

}

package by.vova.server.service;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import by.vova.server.dao.EmployeeDAO;
import by.vova.server.dao.EmployeeDAOImpl;
import by.vova.server.domain.Employee;

public class EmployeeServiceImplTest {

	private static EmployeeServiceImpl service;
	private static EmployeeDAO employeeDAO;
	private static Integer id;
	private Employee employee = null;
	private List<Employee> employees = null;
	
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		id = 1;
		employeeDAO = mock(EmployeeDAOImpl.class);
		service = new EmployeeServiceImpl(employeeDAO);
	}

/*-------------------------------------------------------------------------------------*/	
	
	@Test
	public void testAddEmployee() {
		when(employeeDAO.addEmployee(employee)).thenReturn(employee);
		assertSame(employee, service.addEmployee(employee));
		verify(employeeDAO).addEmployee(employee);
	}

	@Test
	public void testListEmployees() {
		when(employeeDAO.listEmployees()).thenReturn(employees);
		assertSame(employees, service.listEmployees());
		verify(employeeDAO).listEmployees();
	}

	@Test
	public void testGetEmployee() {
		when(employeeDAO.getEmployee(id)).thenReturn(employee);
		assertSame(employee, service.getEmployee(id));
		verify(employeeDAO).getEmployee(id);
	}

	@Test
	public void testRemoveEmployeeInteger() {
		when(employeeDAO.removeEmployee(id)).thenReturn(employee);
		assertSame(employee, service.removeEmployee(id));
		verify(employeeDAO).removeEmployee(id);
	}

	@Test
	public void testRemoveEmployeeEmployee() {
		when(employeeDAO.removeEmployee(employee)).thenReturn(employee);
		assertSame(employee, service.removeEmployee(employee));
		verify(employeeDAO).removeEmployee(employee);
	}

}

package by.vova.server.service;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import by.vova.server.dao.DepartmentDAO;
import by.vova.server.dao.DepartmentDAOImpl;
import by.vova.server.domain.Department;
import by.vova.server.domain.Employee;

public class DepartmentServiceImplTest {

	private static DepartmentServiceImpl service;
	private static DepartmentDAO departmentDAO;
	private static Integer id;
	private Department department;
	
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		id = 1;
		departmentDAO = mock(DepartmentDAOImpl.class);
		service = new DepartmentServiceImpl(departmentDAO);
	}

/*-------------------------------------------------------------------------------------*/	
	/*1*/
	@Test
	public void testAddDepartment() {
		when(departmentDAO.addDepartment(department)).thenReturn(department);
		
		Department result1 = service.addDepartment(department);
		assertSame(department, result1);
		verify(departmentDAO).addDepartment(department);
	}
	
	/*2*/
	@Test
	public void testListDepartment() {
		List<Department> expectedObject2 = null;
		when(departmentDAO.listDepartments()).thenReturn(expectedObject2);
		
		List<Department> result2 = service.listDepartment();
		assertSame(expectedObject2, result2);
		verify(departmentDAO).listDepartments();
	}
	
	/*3*/
	@Test
	public void testRemoveDepartmentWithId() {
		Integer departmentId = 1;
		when(departmentDAO.removeDepartment(departmentId)).thenReturn(department);
		
		Department result3 = service.removeDepartment(departmentId);
		assertSame(department, result3);
		verify(departmentDAO).removeDepartment(departmentId);
	}
	
	/*4*/
	@Test
	public void testRemoveDepartmentWithObject() {
		when(departmentDAO.removeDepartment(department)).thenReturn(department);
		
		Department result4 = service.removeDepartment(department);
		assertSame(department, result4);
		verify(departmentDAO).removeDepartment(department);
	}
	
	/*5*/
	@Test
	public void testGetDepartment() {
		Department expectedObject5 = null;
		when(departmentDAO.getDepartment(id)).thenReturn(expectedObject5);
		
		Department result5 = service.getDepartment(id);
		assertSame(expectedObject5, result5);
		verify(departmentDAO).getDepartment(id);
	}
	
	/*6*/
	@Test
	public void testGetEmployeesInDepartment() {
		List<Employee> expectedObject6 = null;
		when(departmentDAO.getEmployeesInDepartment(id)).thenReturn(expectedObject6);
		
		List<Employee> result6 = service.getEmployeesInDepartment(id);
		assertSame(expectedObject6, result6);
		verify(departmentDAO).getEmployeesInDepartment(id);
	}
	
}

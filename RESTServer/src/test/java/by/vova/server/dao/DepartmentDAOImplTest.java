package by.vova.server.dao;


import static org.junit.Assert.*;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import by.vova.server.domain.Department;
import by.vova.server.domain.Employee;


/*будем проверять на встроенной субд - интеграционное тестирование*/
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:test-data.xml"})

public class DepartmentDAOImplTest {
	
	private Department department;
	
	@Autowired
	private DepartmentDAO departmentDAO;
	@Autowired
	private SessionFactory sessionFactory;
	
	
/*-------------------------------------------------------------------------*/
	
	/*1*/
	@Test
	@Transactional
	public void testAddDepartment() {
		
		Session session = sessionFactory.getCurrentSession();
		
		department = new Department("Фиолетовый");
		
		/*используем тестируемый метод*/
		departmentDAO.addDepartment(department);
		
		/*проверяем добавился ли объект*/
		List<Department> result = (List<Department>)session.createSQLQuery("select * from departments d where name = 'Фиолетовый';").addEntity(Department.class).list();
		
		assertTrue(result.contains(department));
	}

	/*2*/
	/*проверяем наличие заранее созданных объектов в ответе(результате) метода*/
	@Test
	@Transactional
	public void testListDepartments() {
		
		Session session = sessionFactory.getCurrentSession();
		
		session.save(new Department("test department1", 999));
		session.save(new Department("test department2", 543));
		session.save(new Department("test department3", 111));
		
		/*используем тестируемый метод*/
		List<Department> result = departmentDAO.listDepartments();
		
		/*проверяем прочитались ли объеты*/
		List<Department> tmp = (List<Department>)session.createSQLQuery("select * from departments d;").addEntity(Department.class).list();
		
		assertEquals(result, tmp);
		assertFalse(result == tmp);//показывает, что объекты разные
		
	}

	/*4*/
	@Test
	@Transactional
	public void testGetDepartment() {
		
		Session session = sessionFactory.getCurrentSession();
		
		/*подготавливаем данные.*/
		department = new Department("Синий");
		session.save(department);
		
		/*используем тестируемый метод*/
		Department result = departmentDAO.getDepartment(department.getId());
		
		assertTrue(department.equals(result));
		
	}
	
	/*5*/
	@Test
	@Transactional
	public void testGetEmployeesInDepartment() {
		
		Session session = sessionFactory.getCurrentSession();
		
		Department department = new Department("Перламутровый отдел", 1021);
		
		Employee employee1 = new Employee("Вася", "Иванов", "Петрович", 1000, department);
		Employee employee2 = new Employee("Владимир", "Путин", "Владимирович", 500, department);
		Employee employee3 = new Employee("Саша", "Пушкин", "Сергеевич", 600, department);
		
		session.save(department);
		session.save(employee1);
		session.save(employee2);
		session.save(employee3);
		
		session.refresh(department);
		
		List<Employee> result = departmentDAO.getEmployeesInDepartment(department.getId());
		
		/*проверяем полученный результат*/
		int j = 0;
		if (result.contains(employee1))  j++;
		if (result.contains(employee2))  j++;
		if (result.contains(employee3))  j++;
		
		assertTrue(3 == j);
	}
	
	/*6*/
	@Test
	@Transactional
	public void testRemoveDepartmentWithId() {
		
		Session session = sessionFactory.getCurrentSession();
		
		department = new Department("Сиреневый");
		session.save(department);
		
		Department result = departmentDAO.removeDepartment(department.getId());
		
		assertTrue(department.equals(result));
		
	}
	
	/*7*/
	@Test
	@Transactional
	public void testRemoveDepartmentWithObject() {
		
		Session session = sessionFactory.getCurrentSession();
		
		department = new Department("Пурпурный");
		session.save(department);
			
		Department result = departmentDAO.removeDepartment(department);
		assertEquals(department, result);
			
	}
		
	
}

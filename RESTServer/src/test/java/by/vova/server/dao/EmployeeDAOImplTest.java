package by.vova.server.dao;

import static org.junit.Assert.*;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import by.vova.server.domain.Department;
import by.vova.server.domain.Employee;

/*будем проверять на встроенной субд - интеграционное тестирование*/
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:test-data.xml"})

public class EmployeeDAOImplTest {

	private static final Logger logger = LoggerFactory.getLogger(EmployeeDAOImplTest.class);
	
	@Autowired
	private EmployeeDAO employeeDAO;
	@Autowired
	private SessionFactory sessionFactory;
	
/*--------------------------------------------------------------------------------------------------------------------------------*/
	
	@Test
	@Transactional
	public void testAddEmployee() {
		
		Session session = sessionFactory.getCurrentSession();
		
		/*создаём отдел и добавляем его в БД */
		Department department = new Department("testName");
		session.save(department);
		
		/*создаём сотрудника и добавляем его в БД тестируемым методом*/
		Employee employee = new Employee("testFirstname", "testLastname", "testPatronymic", 455, department);
		employeeDAO.addEmployee(employee);
		
		/*проверяем добавился ли сотрудник в бд*/
		/*получаем id сотрудника сторонним способом*/
		@SuppressWarnings("unchecked")
		List<Employee> result = (List<Employee>)session.createSQLQuery("select * from employees e where firstname = 'testFirstname';").addEntity(Employee.class).list();
		
		/*проверяем добавился ли сотрудник*/
		assertTrue(result.contains(employee));
	}
	
	@Test
	@Transactional
	public void testListEmployees() {
		
		Session session = sessionFactory.getCurrentSession();
		
		/*создаём отдел и добавляем его в БД */
		Department department = new Department("testName");
		session.save(department);
		
		/*создаём сотрудника и добавляем его в БД тестируемым методом*/
		Employee employee1 = new Employee("testFirstname1", "testLastname1", "testPatronymic1", 455, department);
		Employee employee2 = new Employee("testFirstname2", "testLastname2", "testPatronymic2", 456, department);
		session.save(employee1);
		session.save(employee2);
		
		/*получаем список тестируемым методом*/
		List<Employee> result = employeeDAO.listEmployees();
		
		/*проверяем есть ли в полученном результате нужные объекты.*/
		int k = 0;
		if (result.contains(employee1)) k++;
		if (result.contains(employee2)) k++;
		logger.info("k="+k);
		assertTrue(2 == k);
	}
	
	@Test
	@Transactional
	public void testGetEmployee() {
		
		Session session = sessionFactory.getCurrentSession();
		
		/*создаём отдел и добавляем его*/
		Department department = new Department("testName");
		session.save(department);
		
		/*создаём сотрудника и добавляем его*/
		Employee employee = new Employee("testFirstname", "testLastname", "testPatronymic", 455, department);
		session.save(employee);
		
		Employee result = employeeDAO.getEmployee(employee.getId());
		
		assertEquals(employee, result);
	}

	@Test
	@Transactional
	public void testRemoveEmployeeInteger() {
		
		Session session = sessionFactory.getCurrentSession();
		
		Department department = new Department("ntestName");
		session.save(department);
		
		Employee employee = new Employee("firstname", "lastname", "patronymic", 1110, department);
		session.save(employee);//чтобы появился id, только после этого можно связывать объекты
		
		logger.info("0,5>"+employee.getDepartment());
		logger.info("1>>>"+department.getEmployees());//null - т.к. не было рефреша связи не обновились

		/*удаляем объект тестируемым методом*/
		employeeDAO.removeEmployee(employee.getId());
		
		/*проверяем есть ли в полученном результате удалённый объект.*/
		List<Employee> result  = (List<Employee>)session.createSQLQuery("SELECT * FROM employees e;").addEntity(Employee.class).list();
		for (Employee emp: result) {
			logger.info(">>>>>>>>>>>>>>>>: "+emp);
		}
		logger.info(">>>>>>>>>>>>>>>>result: "+result);
		assertFalse(result.contains(employee));
		
	}

	@Test
	@Transactional
	public void testRemoveEmployeeEmployee() {
		
		Session session = sessionFactory.getCurrentSession();
		
		/*создаём отдел и добавляем его*/
		Department department = new Department("testName");
		session.save(department);
		
		/*создаём сотрудника и добавляем его*/
		Employee employee = new Employee("testFirstname", "testLastname", "testPatronymic", 455, department);
		session.save(employee);
		//session.refresh(employee); //Обновляет состояние данного экземпляра в базе данных
		
		employeeDAO.removeEmployee(employee);
		
		/*проверяем есть ли в полученном результате удалённый объект.*/
		@SuppressWarnings("unchecked")
		List<Employee> result  = (List<Employee>)session.createSQLQuery("SELECT * FROM employees e;").addEntity(Employee.class).list();
		for (Employee emp: result) {
			logger.info(">>>>>>>>>>>>>>>>result: "+emp);
		}
		logger.info(">>>>>>>>>>>>>>>>result: "+result);
		assertFalse(result.contains(employee));
		
	}

}

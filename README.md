# README #

### Краткое описание проекта ###

Это серверная часть небольшого проекта.
Это веб-приложение отдаёт информацию посредством веб-сервиса.
Непосредственно в сервеной части используются:
- java 1.7
- maven
- Spring (MVC, IoC, Security) /version 3.0.5.RELEASE
- jackson (Для реализации веб-сервиса REST)
- JUnit (тесты)
- mockito (для тестирования)

для запуска веб-приложения необходимо сделать следующее:

 1.Установить
 - java версии 1.7 или выше (http://www.oracle.com/technetwork/java/javase/downloads)
 - maven (https://maven.apache.org/download.cgi)
 - сервер приложений TomCat (https://tomcat.apache.org/download-70.cgi)
 
2. Запустить на сервере
 - создать в TomCat пользователя с правами на развёртывание веб-приложения.
 
 Для развертывания приложения через Maven необходимо присвоить пользователю роль manager-script.
 Для этого в файле tomcat-users.xml (находится в папке conf, где установлен Tomcat) создаем нужную роль manager-script и пользователя с этой ролью.
  <role rolename="manager-script"/>
  <user username="admin" password="mypassword" roles="manager-script"/>
  
 - добавить настройки в maven.
   В конфигурационном файле Maven settings.xml (находится в папке conf, где установлен Maven) в разделе servers добавляем сервер Tomcat, с указанием логина и пароля пользователя.
  <servers>
    <server>
      <id>tomcat</id>
      <username>admin</username>
      <password>mypassword</password>
    </server>
  </servers>
  
 
 - в папке с этим ,скачанным, веб-приложением ввести в командной строке
     mvn tomcat7:deploy   - это развернёт веб-приложение на сервере приложений, 
     mvn tomcat7:redeploy   - если необходимо переустановить  приложение,
     mvn tomcat7:undeploy   - если необходимо удалить развернутое ранее приложение.
	 
3. Теперь можно зайти на страницу веб-приложения по адресу http://localhost:8080/RESTServer/
и увидеть страницу с кодом 404, так как страницы приветствия не существует(добавлю попозже).
 Путь может быть другим, в зависимости от настройки TomCat. Может быть другой порт.
 
 Веб-сервисы доступны по адресу 
 http://localhost:8080/RESTServer/rs/department - получаем список отделов.(уже не используется в клиенте).
 http://localhost:8080/RESTServer/rs/department/as - получаем список отделов с высчитанной средней зарплатой.
 http://localhost:8080/RESTServer/rs/department/plus{id} - получаем список сотрудников в отделе с определённым id.
 остальные возможности, такие как создание и модификация сущностей, зааннотированы - ввиду отсутствия на это требований к приложению.